#Auteur : Thomas SANDIER

class SimpleComplexCalculator:
    def complex_sum(self, complex1, complex2):
        real_part = complex1[0] + complex2[0]  # Calcul de la partie réelle
        imaginary_part = complex1[1] + complex2[1]  # Calcul de la partie imaginaire
        return [real_part, imaginary_part]  # Retourne le résultat sous forme d'une liste

    def complex_subtract(self, complex1, complex2):
        real_part = complex1[0] - complex2[0]  # Calcul de la partie réelle
        imaginary_part = complex1[1] - complex2[1]  # Calcul de la partie imaginaire
        return [real_part, imaginary_part]  # Retourne le résultat sous forme d'une liste

    def complex_multiply(self, complex1, complex2):
        real_part = complex1[0] * complex2[0] - complex1[1] * complex2[1]  # Calcul de la partie réelle
        imaginary_part = complex1[0] * complex2[1] + complex1[1] * complex2[0]  # Calcul de la partie imaginaire
        return [real_part, imaginary_part]  # Retourne le résultat sous forme d'une liste

    def complex_divide(self, complex1, complex2):
        denominator = complex2[0] ** 2 + complex2[1] ** 2  # Calcul du dénominateur
        real_part = (complex1[0] * complex2[0] + complex1[1] * complex2[1]) / denominator  # Calcul de la partie réelle
        imaginary_part = (complex1[1] * complex2[0] - complex1[0] * complex2[1]) / denominator  # Calcul de la partie imaginaire
        return [real_part, imaginary_part]  # Retourne le résultat sous forme d'une liste

# Tester la classe
complex1 = [4.67, 5.89]  # Premier nombre complexe
complex2 = [2.34, 3.45]  # Deuxième nombre complexe

calculator = SimpleComplexCalculator()

print("Somme :", calculator.complex_sum(complex1, complex2))  # Affiche la somme des deux nombres complexes
print("Différence :", calculator.complex_subtract(complex1, complex2))  # Affiche la différence des deux nombres complexes
print("Produit :", calculator.complex_multiply(complex1, complex2))  # Affiche le produit des deux nombres complexes
print("Division :", calculator.complex_divide(complex1, complex2))  # Affiche la division des deux nombres complexes