# tp1_ex2

Le code ci-dessus définit une classe appelée SimpleComplexCalculator qui implémente les opérations mathématiques de base sur les nombres complexes. Les opérations incluses sont l'addition, la soustraction, la multiplication et la division de nombres complexes.

La classe SimpleComplexCalculator a quatre méthodes: complex_sum, complex_subtract, complex_multiply et complex_divide. Chacune de ces méthodes prend deux nombres complexes en entrée et retourne le résultat de l'opération correspondante sous forme d'une liste.

Le code teste ensuite la classe en créant deux nombres complexes (complex1 et complex2) et en utilisant les méthodes de la classe pour effectuer les opérations sur ces nombres. Les résultats sont ensuite affichés à l'aide de la fonction print.
